
// Spreadsheet
const spreadsheet = SpreadsheetApp.getActive();
const state_sheet = spreadsheet.getSheets()[STATE_SHEET];
var unused_story_id = 0;

/*!
 * Udate the STATE table with content received
 */
function update_received(step) {

  var form_content = spreadsheet.getSheets()[FORMS_STARTING_INDEX + step].getSheetValues(1, 1, MAX_CORDEES, MAX_RSP_COLUMNS);
  var state_content = state_sheet.getSheetValues(START_STATE_TABLE, 1, MAX_CORDEES, STATE_TABLE_SIZE);

  // Get name column index
  var mail_index = Array(10).fill().map((_, i) => form_content[0][i]).indexOf(MAIL_QUESTION_STRING[1]);
  if (mail_index == -1)
  {
    var mail_index = Array(10).fill().map((_, i) => form_content[0][i]).indexOf(MAIL_QUESTION_STRING[0]);

    if (mail_index == -1)
    {
      console.error("update_received: STEP " + step + ": Couldn't find mail index ! Aborting...");  
      return;
    }
  }

  var mails = state_content.map(i => i[0]);

  // If step is even, => We expect a sentence, otherwise a creation
  if (step % 2 == 0)
    var string = SENTENCE_PUBLISH_QUESTION_STRING;
  else
    var string = CREATION_PUBLISH_QUESTION_STRING;
  
  var publish_index = get_question_column(form_content, string);

  if (publish_index == -1)
  {
    console.error("update_received: STEP " + step + ": Couldn't find publish index ! Aborting...");  
    return;
  }

  // For each input, update received table
  var to_be_updated = mails.filter(i => i).map(i => "");
  form_content.slice(1).map(line => {

    // If line is empty, give up
    if(!line[mail_index])
      return;

    // Try to find the line number with the cordee
    var index = mails.indexOf(line[mail_index])

    if (index == -1)
    {
      console.error("Couldn't find email address: " + line[mail_index] + " (Entire line: " + line + ")");
      return;
    }

    // Get story id (or create it if it's the first step)
    if(step == 0)
    {
      var story_id = unused_story_id.toString().padStart(4, "0");
      unused_story_id++;
    }
    else
    {
      var story_id = state_content[index][2*step + 1].slice(0, 4);
    }

    to_be_updated[index] = story_id + "-" + line[publish_index];
  })
  
  try {
    state_sheet.getRange(START_STATE_TABLE, 2*step+3, to_be_updated.length).setValues(to_be_updated.map(i => [i]));
  } catch (e) {
    console.log("why ?");
  }

}

/*!
 * Prepare stuff to be sent at next step
 */
function prepare_sending(step, start_index = 0) {

  var data = state_sheet.getRange(START_STATE_TABLE, 1, MAX_CORDEES, NB_STEPS*2+1)
    .getValues().filter(i => i[0]);

  var nb_cordee = data.length;

  var to_be_sent = Array(nb_cordee - start_index);

  while (true)
  {
    var available_stories = data.map(i => i[2*(step-1) + 2]).filter(i => i);
    var remaining_stories = available_stories.slice();

    try {
      data.slice(start_index).map((line, index) => {
        var mail = line[0];
        var previous_stories = [];
        for (var i = 0; i<step; i++)
        {
          var col;
          if (i <= 1)
            col = 2*i + 2;
          else
            col = 2*i + 1;
          previous_stories.push(line[col].slice(0, 4));
        }
        var found_str;
        var give_up_counter = 0;
        var list;
        var random_number = 0;
        do
        {
          give_up_counter++;
          if (give_up_counter > 10)
            throw 'No possible solution found after 10 tries';
          
          if (remaining_stories.length > 0)
            var list = remaining_stories;
          else
            var list = available_stories;

          random_number = Math.floor(Math.random() * list.length);
          found_str = list[random_number];

          if (typeof found_str === 'undefined')
            console.log("undefined");
          var id = found_str.slice(0, 4);
        } while(previous_stories.includes(id));

        if (remaining_stories.length > 0)
          list.splice(random_number,1);
        to_be_sent[index] = found_str;
      })
      // We found a valid solution for all things to be sent !
      break;
    }
    catch(e) {
      console.warn("STEP " + step + ": Couldn't find a valid solution.")
    }
  }

  state_sheet.getRange(START_STATE_TABLE + start_index, 2*step + 2, nb_cordee - start_index).setValues(to_be_sent.map(i => [i]));

/*  state_sheet.getRange(START_STATE_TABLE + 1, 2*(step-1) + 3, MAX_CORDEES).copyTo(
    state_sheet.getRange(START_STATE_TABLE, 2*(step) + 2, MAX_CORDEES)
  );

  state_sheet.getRange(START_STATE_TABLE, 2*(step-1) + 3).copyTo(
    state_sheet.getRange(START_STATE_TABLE + nb_cordee - 1, 2*(step) + 2)
  );

  // Check that no cell is empty, otherwise fill it with a random value
  var sent_values = state_sheet.getRange(START_STATE_TABLE, 2*(step) + 2, nb_cordee).getValues();
  var received_values = state_sheet.getRange(START_STATE_TABLE, 2*(step-1) + 3, nb_cordee).getValues();
  for (var i = 0; i < nb_cordee; i++)
  {
    if(!sent_values[i][0])
    {
      var j = i + 7;
      var next_value = "";
      do
      {
        j = (j+1) % nb_cordee;
        next_value = received_values[j][0];
      } while(next_value == "");

      sent_values[i][0] = next_value;  
    }
  }
  */
//  state_sheet.getRange(START_STATE_TABLE, 2*(step) + 2, nb_cordee).setValues(sent_values);
}

function send_mails(step, start_index = 0) {
  
  state_sheet.getRange(START_STATE_TABLE + start_index, 1, MAX_CORDEES, 2*step + 3)
    .getValues().filter(i => i[0]).map(line => {

    var mail = line[0];
    var input = line[2*step + 1].slice(5);

    if (step % 2 != 0)
      var next_step = "réaliser quelque chose (une vidéo, une photo, etc) mettant en scène la phrase ci-dessous:"
    else
      var next_step = "décrire par une phrase le contenu que tu trouveras en suivant le lien ci-dessous:"

    var body = `
Hello !

La prochaine étape consiste à ` + next_step + `
    
    ` + input + `

Tu peux poster le résultat ici: ` + FORMS_ADDRESSES[step] + `

Tu dois l'avoir fait avant ` + DEAD_LINES[step] + ` pour que ce soit pris en compte !

Bon amusement :-)

L'équipe Xpitel !
`;
    console.log("Sending mail\n", mail, "Xpitel: étape " + (step + 1), body, {'name': 'Services Xpitel'});
    GmailApp.sendEmail(mail, "Xpitel: étape " + (step + 1), body, {'name': 'Services Xpitel'});
  });

  // Update "to be sent" state to "sent"
  state_sheet.getRange(START_STATE_TABLE - 1, 2*step + 2).setValue("Sent");

}

function get_address_index(state_content, address) {
  return state_content.map(i => i[0]).indexOf(address);
}

function get_question_column(content, question) {
    return Array(MAX_RSP_COLUMNS).fill().map((_, i) => content[0][i]).indexOf(question);
}

function update_email_validation_in_forms() {

  var content = spreadsheet.getSheets()[FORM1_SHEET].getSheetValues(1, 1, MAX_CORDEES, MAX_RSP_COLUMNS);
  // Get name column index
  var column_index = Array(10).fill().map((_, i) => content[0][i]).indexOf(MAIL_QUESTION_STRING[0]);

  var mails = content.slice(1).map(i => i[column_index].toLowerCase()).filter(i => i);

  // Remove entries that appears twice
  mails = [...new Set(mails)];
  console.log(mails);

  set_mail_validation(mails);

  return mails;
}

function set_mail_validation(mails) {

  // Insert them in mail validations (in all next forms)
  // Get the item concerning cordee name question
  regex_mails = prepare_regex(mails);
  for (i = 1; i < FORMS_IDS.length; i++)
  {
      var item = FormApp.openById(FORMS_IDS[i]).getItems().filter((i) => i.getIndex() == 0)[0];

      // Update the text validation based on existing name list
      var textValidation = FormApp.createTextValidation()
        .setHelpText("L'adresse email entrée ne correspond pas à celle utilisée précédemment")
        .requireTextMatchesPattern(regex_mails)
        .build();
      item.asTextItem().setValidation(textValidation);    
  }
}

function prepare_regex(names) {
  var rsp = "^(";
  for(i=0; i < names.length - 1; i++)
  {
    var name = names[i].replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '|';
    first_letter = name[0].toLowerCase();
    name = "(" + first_letter + "|" + first_letter.toUpperCase() + ")" + name.slice(1);
    rsp += name;
  }
  
  rsp += names[names.length - 1].replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + ')$';
  
  return rsp;
}


function test() {
  prepare_sending(3);
}