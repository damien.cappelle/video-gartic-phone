
const MAX_CORDEES = 1000;
const MAX_RSP_COLUMNS = 3;
const MAIL_QUESTION_STRING = ["Quelle adresse email ta cordée va-t-elle utiliser ?", "Ton adresse email ?"];
const CREATION_PUBLISH_QUESTION_STRING = "Ta création ?";
const SENTENCE_PUBLISH_QUESTION_STRING = "Ta phrase ?";
const FORM1_SHEET = 1;
const FORMS_STARTING_INDEX = 1;
const STATE_SHEET = 0;
const START_STATE_TABLE = 6;
const STATE_TABLE_SIZE = 2*8+1;
const FORMS_IDS = [
  "1JTy0ZRiDgZU-wv69ECf68tU0rnBo_hq0HWGdI55AFwE",
  "1cRnWufaJ501f6qVluhw0KiYasQbYIiB2midqvqsq5_Y",
  "1AjH6lOi8mBvrAYYXUaUZ-zhnA6Namzmux12lVjz-BJQ",
  "1lGiudTL91alzHvVNKFQfNyFw9jwTVJvC_Sg_2BJlFSc",
  "1X1AHsGXFT7PNtvHDX09wv-J4jqywktaehlbrHF4il98",
  "1AXidJ1VCxqAx8olJAJQtewUIbsatII0sp8HIDxWdF5A",
  "1TLeRIpfeOgoEP6wrQcCCputOaSWeYtevOrf-9thmFcE",
  "1WQuh7Hr69ku3cl2yNozdmtuH4CELWjZCAfGIspfqX5c"
  ];
const FORMS_ADDRESSES = [
  "https://forms.gle/XqqzWAcFzVowecZe6",
  "https://forms.gle/S6LLFBfQuCLvEFS17",
  "https://forms.gle/fGBTf82TPgBMWbav8",
  "https://forms.gle/VyVvV8HfBK1juxj77",
  "https://forms.gle/71FYTwaH9Lgtg7es6",
  "https://forms.gle/9Mr6WrKzq6smCS1Y6",
  "https://forms.gle/xb8QjeR8ZSfXBp8TA",
  "https://forms.gle/nTJXtTqoURiVt8Jw6"
]
const NB_STEPS = 8;

const FINAL_DOC_ID = "12t-CgQibc5jDOZtuq7_dSmb3Y073T4h7D8Lcc9IdAlI";

const DEAD_LINES = [
  "11h",
  "11h30",
  "12h",
  "12h30",
  "14h15",
  "14h45",
  "15h15",
  "15h45"
];