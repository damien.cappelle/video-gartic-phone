


function reset() {

  // Security: check that reset is armed
  state_range = state_sheet.getRange(2, 1);
  var val = state_range.getValue();
  if (state_range.getValue() != "reset")
  {
    console.error("Reset function started but not armed... Aborting!");
    return;
  }

  // Reset current step
  state_range.setValue(1);

  // Disable all forms except 1st
  for (i = 1; i < FORMS_IDS.length; i++)
  FORMS_IDS.slice(1).map(form_id => {
    FormApp.openById(form_id).setAcceptingResponses(false);
  });

  FormApp.openById(FORMS_IDS[0]).setAcceptingResponses(true);

  // Clear all responses
  FORMS_IDS.map(form_id => {
    FormApp.openById(form_id).deleteAllResponses();
  });

  for(var i = 0; i < NB_STEPS; i++) {
    sheet = spreadsheet.getSheets()[FORMS_STARTING_INDEX + i];
    if (sheet)
      spreadsheet.getSheets()[FORMS_STARTING_INDEX + i].clear();
    else
      console.warn("No sheet found with index " + (FORMS_STARTING_INDEX + i) + ": passing");
  }

  // Change all "Sent" to "To be sent"
  for(var i = 0; i < NB_STEPS; i++) {
    state_sheet.getRange(START_STATE_TABLE - 1, i*2 + 2).setValue("To be sent");
  }

  // Clear State table
  state_sheet.getRange(START_STATE_TABLE, 1, MAX_CORDEES, NB_STEPS*2 + 1).clearContent();

}

function process_step() {

  // Get current step:
  var str_current_state = state_sheet.getRange(2, 1).getValue();
  var hr_current_state = Number(str_current_state);
  
  if (isNaN(hr_current_state))
  {
    console.error("Invalid current state (" + str_current_state + "): Aborting!");
    return;
  }
  var current_state = hr_current_state - 1;

  // Post processing of previous step
  console.log("Post processing step " + hr_current_state);

  if(current_state == 0)
  {
    // Update next form with valid email adresses
    var mails = update_email_validation_in_forms();

    // Create table with all posts
    state_sheet.getRange(START_STATE_TABLE, 1, mails.length).setValues(mails.map(i => [i]));
  }

  update_received(current_state);
  
  // Disable previous form
  //FormApp.openById(FORMS_IDS[current_state]).setAcceptingResponses(false);

  if (current_state >= 7)
  {
    console.log("It's finished !");
    return;
  }

  // Update current step
  current_state++;
  hr_current_state++;
  state_sheet.getRange(2,1).setValue(hr_current_state);
  
  // Enable next form
  //FormApp.openById(FORMS_IDS[current_state]).setAcceptingResponses(true);

  // Pre processing of next step
  prepare_sending(current_state);
  send_mails(current_state);
}

function insert_new_cordee() {
  var new_cordee_mails = ["guanac92@hotmail.com"];

  var current_step = state_sheet.getRange(2, 1).getValue() - 1;
  if (current_step <= 0)
  {
    console.error("Can't insert new email address at step 0");
  }


  // Insert new mail in sheet
  var existing_mails = state_sheet.getRange(START_STATE_TABLE, 1, MAX_CORDEES).getValues()
    .map(i => i[0]).filter(i => i);
  nb_cordee = existing_mails.length;
  state_sheet.getRange(START_STATE_TABLE + nb_cordee, 1, new_cordee_mails.length).setValues(new_cordee_mails.map(i => [i]));

  existing_mails.push(...new_cordee_mails);

  // Update mail validation in forms
//  set_mail_validation(existing_mails);

  // Prepare stuff to send
  prepare_sending(current_step, nb_cordee);

  // Send mail to cordée with one sentence
  send_mails(current_step, nb_cordee);

}

function send_conclusion() {

  var data = state_sheet.getRange(START_STATE_TABLE, 1, MAX_CORDEES, NB_STEPS*2+1).getValues().filter(i => i[0]);

  var result = data.map(i => [i[2].slice(0, 4), i[2].slice(5)]);

  for (var step = 1; step < NB_STEPS; step++)
  {
    var sent_elems = data.map(i => i[2*step + 1].slice(0, 4));
    var received_elems = data.map(i => i[2*step + 2]);

    result.map(p => {
      var story_id = p[0];
      var index = sent_elems.indexOf(story_id);
      if(index == -1)
        p.push("")
      else
        p.push(received_elems[index].slice(5));      
      return p;
    });
  }

  SpreadsheetApp.openById(FINAL_DOC_ID).getSheets()[0].
    getRange(2,1, result.length, NB_STEPS).setValues(result.map(i => i.slice(1)));
}
